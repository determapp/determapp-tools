const { runBuildCommand } = require('./run-build-command.js')

runBuildCommand('npm run prepare').catch(function (ex) {
  console.warn(ex)
  process.exit(1)
})
