const { readdirSync, readFileSync, writeFileSync } = require('fs');
const { resolve } = require('path');

console.log('get list of packages');

const packagesDir = resolve(__dirname, '../packages');
const packagesList = readdirSync(packagesDir);

for (let i = 0; i < packagesList.length; i++) {
  const packageName = packagesList[i];

  console.log('Process ' + packageName);

  const packageDir = resolve(packagesDir, './' + packageName);
  const packageJsonPath = resolve(packageDir, './package.json');

  const packageJson = JSON.parse(readFileSync(packageJsonPath));

  packageJson.repository = {
    type: 'git',
    url: 'git+ssh://git@gitlab.com/determapp/determapp-tools.git'
  };

  packageJson.bugs = {
    url: 'https://gitlab.com/determapp/determapp-tools/issues'
  };

  packageJson.homepage = 'https://gitlab.com/determapp/determapp-tools/tree/master/packages/' + packageName + '#README';

  writeFileSync(packageJsonPath, JSON.stringify(packageJson, null, 2));
}

console.log('done');
