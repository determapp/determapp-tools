const { runBuildCommand } = require('./run-build-command.js')

runBuildCommand('npm install').catch(function (ex) {
  console.warn(ex)
  process.exit(1)
})
