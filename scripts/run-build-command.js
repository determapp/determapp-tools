const _ = require('lodash')
const { exec } = require('child_process')
const pLimit = require('p-limit')
const path = require('path')
const dependencies = require('../dependencies.json')

const limit = pLimit(4)

module.exports.runBuildCommand = function (command) {
  const runForModule = _.memoize(function(module) {
    const moduleDependencies = dependencies[module]

    if (!moduleDependencies) {
      throw new Error('invalid module: ' + module)
    }

    return Promise.all(moduleDependencies.map((item) => runForModule(item))).then(() => {
      return limit(function() {
        return new Promise(function (resolve, reject) {
          console.log('run for module ' + module)

          exec(command, {
            cwd: path.join(__dirname, '../packages/node_modules/' + module),
            env: process.env
          }, function (err, stdout, stderr) {
            if (err) {
              console.log('error during processing ' + module)

              reject(err)
            } else {
              resolve()
            }
          })
        })
      })
    })
  })

  return Promise.all(Object.keys(dependencies).map((module) => runForModule(module)))
}
