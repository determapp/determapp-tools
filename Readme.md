# DetermApp-Tools

A collection of Tools for DetermApp to create and publish determination keys.
For end user information and executable downloads, see <https://determapp.de/tools/>.

## Features

- create and edit determination keys
- export determination keys as HTML files

## Structure

This is a monorepo which contains multiple packages. For managing this, [Lerna](https://github.com/lerna/lerna/blob/master/README.md) is used.
