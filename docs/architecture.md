
## Architecture

![architecture of the DetermApp-Tools](./architecture.png)

### file format

- saving actions (like adding question, updating text, ...)
- one action per line
- actions are saved as JSON

### other packages

This components are used by multiple other components so it is not easily possible to show them in the graph.

#### @determapp/actions

- converts actions (see file format) to an status
- describes all actions and the status created by them
- provides functions to validate actions
- provides helper functions to create actions objects
- provides helpers to use the created status object

#### @determapp/id

- id generation and validation
- ids are used for nearly everything
    - project ids
    - result ids
    - question ids
    - answer ids
    - ...

#### @determapp/lock

- simple lock mechanism

#### @determapp/serve-tgz

- express route handler to serve the content of an archive
- uses for the viewer server and the editor-http-server
